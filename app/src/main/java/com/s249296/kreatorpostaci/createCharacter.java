package com.s249296.kreatorpostaci;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class createCharacter extends AppCompatActivity {
    String[] races = new String[5];
    ArrayAdapter<String> RaceAdapter;

    String[] classes = new String[4];
    ArrayAdapter<String> classesAdapter;


    String[] genders = new String[3];
    ArrayAdapter<String> GendersAdapter;
    String[] abilities = new String[6];
    int[] ppos = new int[2];
    ArrayAdapter<String> pAbilities, sAbilities;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_character);
        ppos[0] =0;
        ppos[1]= 1;
        races[0] = "Human";
        races[1]  = "Orc";
        races[2] = "Elf";
        races[3]  = "Lizardfolk";
        races[4] = "Undead";
        RaceAdapter =new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,races);
        classes[0] = "Warrior";
        classes[1] = "Rogue";
        classes[2] = "Archer";
        classes[3] = "Mage";
        classesAdapter =  new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,classes);
        genders[0] = "Male";
        genders[1] = "Female";
        genders[2]= "Unspecified";
        GendersAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,genders);
        abilities[0] = "Nothing";
        abilities[1] = "Animal Communication";
        abilities[2] = "Night Vision";
        abilities[3] = "Perception";
        abilities[4] = "Stealth";
        abilities[5] = "Survival";

        pAbilities =  new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item,abilities
        ) {
            @Override
            public boolean isEnabled(int position) {
                return (ppos[0] == position || ppos[1] == position)? false : super.isEnabled(position);
            }
        };
        sAbilities =  new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item,abilities
        ){
            @Override
            public boolean isEnabled(int position) {
                return (ppos[0] == position || ppos[1] == position)? false : super.isEnabled(position);
            }
        };
        Spinner pAbility = findViewById(R.id.pability);
        Spinner sAbility = findViewById(R.id.sability);
        Spinner RacesSpinner = findViewById(R.id.RaceSpinner);
        Spinner classesSpiner = findViewById(R.id.classesSpinner);
        Spinner genderSpinner = findViewById(R.id.GenderSpinner);
        pAbility.setAdapter(pAbilities);
        sAbility.setAdapter(sAbilities);
        RacesSpinner.setAdapter(RaceAdapter);
        classesSpiner.setAdapter(classesAdapter);
        genderSpinner.setAdapter(GendersAdapter);
        pAbility.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ppos[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sAbility.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ppos[1] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Button nextPage = findViewById(R.id.gonext);
        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Character c = Character.getInstance();
                c.setAbility(0, abilities[ppos[0]]);
                c.setAbility(1, abilities[ppos[1]]);
                c.setName(((EditText)findViewById(R.id.editTextTextPersonName)).getText().toString());
                c.setGender(genderSpinner.getSelectedItem().toString());
                c.setRace(RacesSpinner.getSelectedItem().toString());
                c.setClassCh(classesSpiner.getSelectedItem().toString());

                Intent intent = new Intent(getApplicationContext(),PointsGiveAway.class);
                startActivity(intent);
            }
        });
    }
}