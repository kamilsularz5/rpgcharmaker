package com.s249296.kreatorpostaci;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button createNewCaracter = findViewById(R.id.create);
        Button LoadCreatedCharacter = findViewById(R.id.load);

        createNewCaracter.setOnClickListener(v -> {
                Intent intent = new Intent(getApplicationContext(),createCharacter.class);
                startActivity(intent);
                }
        );
        LoadCreatedCharacter.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(),LoadCharacter.class);
            startActivity(intent);
        }
        );




    }
}