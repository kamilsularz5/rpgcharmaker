package com.s249296.kreatorpostaci;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class storytime extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storytime);

        final ConstraintLayout relativeLayout;
        relativeLayout = findViewById(R.id.storyT);
        Character c = Character.getInstance();
        if(c.getClassCh().equals("Rogue")){
            relativeLayout.setBackgroundColor(Color.parseColor("#4d0099"));
            findViewById(R.id.button3).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.editTextTextPersonName2).setBackgroundColor(Color.parseColor("#4d0099"));
        }
        else if(c.getClassCh().equals("Archer")){
            relativeLayout.setBackgroundColor(Color.parseColor("#73cc65"));
            findViewById(R.id.button3).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.editTextTextPersonName2).setBackgroundColor(Color.parseColor("#73cc65"));
        }
        else if(c.getClassCh().equals("Mage")) {
            relativeLayout.setBackgroundColor(Color.parseColor("#0099ff"));
            findViewById(R.id.button3).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.editTextTextPersonName2).setBackgroundColor(Color.parseColor("#0099ff"));
        }


        ((Button) findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Character.getInstance().setStory(
                        ((EditText) findViewById(R.id.editTextTextPersonName2)).getText().toString()
                );
                File f = new File(getFilesDir() + "/"+c.getName()+".txt");

                FileOutputStream fos = null;
                try {
                    fos=openFileOutput(f.getName(),MODE_PRIVATE);
                    fos.write(c.toString().getBytes());
                    Toast.makeText(getApplicationContext(),"Saved to"+f.toString(),Toast.LENGTH_LONG).show();
                }catch (FileNotFoundException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }finally {
                    if(fos!=null){
                        try {
                            fos.close();
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                }

                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }

        });

    }
}